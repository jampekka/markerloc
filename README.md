# Estimate square marker setup

## Usage

Extract markers from `video.mp4` using a camera model, this example uses a model
calibrated for a [pupil-labs](https://pupil-labs.com/) scene camera. Model to use
with the undistorted marker points is saved as `camera_params_rect.pickle`.

    ./extract_markers.py -n camera_params_rect.pickle <(gunzip pupil.fisheye.720p.pickle.gz) video.mp4 markers.npy

Estimate marker orientations relative to marker reference marker with id `0` and save them to `marker_locations.pickle`.
	
    ./markerloc.py -r 0 camera_params_rect.pickle markers.npy marker_locations.pickle
