#!/usr/bin/env python2
from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np
import autograd.numpy as anp
from autograd import jacobian
import pickle
import cv2
import scipy.optimize


from camera_poses import estimate_poses
def pixerrors(diffs):
    diffs = np.sqrt(np.sum(diffs**2, axis=1))
    return diffs

def transform_points(world, rvec, tvec):
    angle = np.linalg.norm(rvec)
    if angle > 1e-6:
        axis = (rvec/angle).reshape(3, 1)
        x, y, z = axis.reshape(-1)
        R = np.array((((0.0, -z, y), (z, 0.0, -x), (-y, x, 0.0))))
        R = np.cos(angle)*np.identity(3) + np.sin(angle)*R + (1 - np.cos(angle))*np.dot(axis, axis.T)
        world = np.inner(R, world).T
    tvec = tvec.reshape(-1)
    return world + tvec

def project_points(world, rvec, tvec, cm, cd):
    #angle = np.linalg.norm(rvec)
    #axis = (rvec/angle).reshape(3, 1)
    #x, y, z = axis.reshape(-1)
    #R = np.array((((0.0, -z, y), (z, 0.0, -x), (-y, x, 0.0))))
    #R = np.cos(angle)*np.identity(3) + np.sin(angle)*R + (1 - np.cos(angle))*np.dot(axis, axis.T)
    #tvec = tvec.reshape(-1)
    #world = np.inner(R, world).T + tvec
    world = transform_points(world, rvec, tvec)
    proj = np.inner(cm, world).T
    proj /= proj[:,-1:]
    return proj[:,:2]

def project_frames(poses, world, cm, cd):
    ests = np.empty((len(poses), len(world), 2))
    #ests = []
    for i, (rvec, tvec) in enumerate(poses):
        est = cv2.projectPoints(world, rvec, tvec, cm, cd)[0].reshape(-1, 2)
        #est = project_points(world, rvec, tvec, cm, cd)
        #ests.append(est)
        ests[i] = est
    #print len(ests[i])
    return np.array(ests)

def reprojection_errors(poses, world, screen, cm, cd):
    ests = project_frames(poses, world, cm, cd)
    screen = screen.reshape(-1, len(world), 2)
    ests -= screen.reshape(-1, len(world), 2)
    """
    center = np.array([cm[0][-1], cm[1][-1]]).reshape(1, 1, 2)
    scale = np.array([cm[0][0], cm[1][1]]).reshape(1, 1, 2)
    offset = screen - center
    offset /= scale
    offset = np.exp(-(np.sum(offset**2, axis=2))*10).reshape(-1, 4, 1)
    """
    # Downweigh eccentric points
    #ests *= offset
    return ests

def estimate_marker_location(screens, poses, mpose, mworld, cm, cd):
    mposes = []
    mscreens = []
    for i, rvec, tvec in poses:
        if i not in screens: continue
        mposes.append((rvec, tvec))
        mscreens.append(screens[i])
    mscreens = np.array(mscreens)
    def errfunc(mpose):
        world = transform_points(mworld*np.exp(mpose[0]), *mpose[1:].reshape(2, 3))
        errors = reprojection_errors(mposes, world, mscreens, cm, cd).ravel()
        #errors /= len(errors)
        return errors
        edges = []
        for i in range(len(world)):
            edge = world[i] - world[(i+1)%len(world)]
            edgelen = np.sqrt(np.sum(edge**2))
            edges.append(edgelen)
        evar = np.var(edges)*5
        ones = np.ones((world.shape[-1], 1))
        tmp = np.ones((4, 4))
        tmp[:,:-1] = world
        det = np.linalg.det(tmp.T)*5
        return errors
        #return np.hstack((errors, [evar, det]))
    
    #jac = jacobian(errfunc)
    """
    print jac(world.ravel())
    jac(world.ravel())
    print "Done"
    """
    
    result = scipy.optimize.least_squares(errfunc, mpose.ravel(), x_scale='jac', max_nfev=100, loss='huber')
    e = errfunc(result.x)
    deviations = e.reshape(-1, 2) #+ np.abs(e[-2]) + np.abs(e[-1])
    return result.x, deviations


def estimate_marker_locations(marker_data, poses, estimated_locations, mdict, cm, cd):
    screens = defaultdict(dict)
    for i, markers in enumerate(marker_data):
        for marker in markers:
            screens[marker['id']][i] = marker['verts']
    
    new_estimated_locations = {}
    errors = {}
    for marker_id, world in estimated_locations.iteritems():
        est, err = estimate_marker_location(screens[marker_id], poses, world, mdict[marker_id], cm, cd)
        new_estimated_locations[marker_id] = est
        errors[marker_id] = err
    return new_estimated_locations, errors

from mpl_toolkits.mplot3d import Axes3D
def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

def test(camera_spec, marker_file, out_file, reference_id=0, frequency_threshold=0.05,
        max_frames=300, error_threshold=5.0, visualize=False):
    camera = pickle.load(open(camera_spec))
    cm, cd = camera['camera_matrix'], camera['dist_coefs']
    marker_geometry = np.array([
        [-1,1,0],
        [1,1,0],
        [1,-1,0],
        [-1,-1,0],
        ]).astype(np.float)*0.05
    mdict = defaultdict(lambda: marker_geometry)
    marker_geometry = marker_geometry[::-1]
    a = np.pi/2*2
    m = marker_geometry
    m[:,0], m[:,1] = (
            np.cos(a)*m[:,0] - np.sin(a)*m[:,1],
            np.sin(a)*m[:,0] + np.cos(a)*m[:,1],
            )

    #from simulate import markersim
    #marker_data = markersim(cm, cd, mdict)
    #marker_data, tposes, tpositions = zip(*(marker_data.next() for i in range(100))); reference_id=0
    omarker_data = np.load(marker_file)
    decim = len(omarker_data)/max_frames + 1
    marker_data = omarker_data[::decim]
    
    screens = defaultdict(dict)
    for i, markers in enumerate(marker_data):
        for marker in markers:
            screens[marker['id']][i] = marker['verts']
    
    max_screens = max((len(s) for s in screens.values()))
    #known_locations = {reference_id: mdict[reference_id]}
    #whitelist = [reference_id, 8, 44, 19, 27, 10, 7]
    #whitelist = [reference_id, 8, 19, 27]
    #whitelist = [reference_id, 8, 44]
    #whitelist = [reference_id, 44, 8, 5, 4, 28, 8]
    estimated_locations = {}
    for marker_id in screens:
            if marker_id == reference_id: continue
            #if marker_id not in whitelist: continue
            if len(screens[marker_id]) < max_screens*frequency_threshold:
                continue
            #estimated_locations[marker_id] = mdict[marker_id]
            estimated_locations[marker_id] = np.zeros(1+3*2)
    min_points = 4
    best_matches = defaultdict(lambda: np.inf)
    #good_locations = {reference_id: mdict[reference_id]}
    good_locations = {reference_id: np.zeros(1+3*2)}
    
    def pointify(mposes):
        return {k: transform_points(mdict[k]*np.exp(t[0]), *t[1:].reshape(2, 3)) for k, t in mposes.iteritems()}
    for i in range(100):
        poses = estimate_poses(marker_data, pointify(good_locations), cm, cd, min_points=min_points)
        min_points = 2*4
        
        new_estimated_locations, errors = estimate_marker_locations(marker_data, poses, estimated_locations, mdict, cm, cd)
        enhanced = 0
        besterr = np.inf; best_k = None
        for k, loc in new_estimated_locations.iteritems():
            error = np.median(pixerrors(errors[k]))
            estimated_locations[k] = loc
            if k in good_locations.keys():
                if error > error_threshold:
                    del good_locations[k]
                else:
                    good_locations[k] = loc
                    print "Known", k, error
                continue
            if error < besterr and len(errors[k]) > max_screens*frequency_threshold:
                besterr = error
                best_k = k
            #best_matches[k] = errors[k]
            #enhanced += 1
             
        if best_k is None:
            print "No markers left"
            break
        if besterr > error_threshold:
            print "Removing marker", best_k, "with error", besterr
            del estimated_locations[best_k]
        else:
            good_locations.update({best_k: estimated_locations[best_k]})
        #del estimated_locations[best_k]

        #all_locations = dict(known_locations, **estimated_locations)
        plt.figure(0)
        plt.clf()
        ax = plt.gcf().add_subplot(111, projection='3d')
        for marker_id, res in pointify(good_locations).iteritems():
            ax.text(res[0,0], res[0,1], res[0,2], str(marker_id))
            ax.plot(res[:,0], res[:,1], res[:,2])
        axisEqual3D(ax)
        plt.figure(1)
        plt.clf()
        i, rots, trans = map(np.array, zip(*poses))
        plt.plot(i, (trans[:,0]), color='black')
        plt.pause(0.1)
        """
        for marker_id, loc in all_locations.iteritems():
            locs = {marker_id: loc}
            tposes = estimate_poses(marker_data, locs, cm, cd, min_points=4)
            i, rots, trans = map(np.array, zip(*tposes))
            plt.plot(i, (trans[:,0]))
        plt.show()
        """
        #if enhanced == 0:
        #    break
    pickle.dump(good_locations, open(out_file, 'w'))
    if visualize: 
        poses = estimate_poses(omarker_data, pointify(good_locations), cm, cd, min_points=min_points)
        i, rots, trans = map(np.array, zip(*poses))
        plt.figure(1)
        plt.clf()
        for d in range(3):
            plt.plot(i, (trans[:,d]), '.-')
        plt.show()

if __name__ == '__main__':
    import argh; argh.dispatch_command(test)
